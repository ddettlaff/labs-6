﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
   public class OwnAdapter : IControlPanel
    {
        IMikrofalowka mikrofala;
        Window window;
        public OwnAdapter(IMikrofalowka mikrofalowka)
        {
            this.window = new WindowWpf(mikrofalowka);
            this.mikrofala = mikrofalowka;
        }
        public Window Window
        {
            get { return window; }
        }
    }
}
