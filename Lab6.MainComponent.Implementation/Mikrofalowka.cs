﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Display.Contract;
using Lab6.MainComponent.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Mikrofalowka : IMikrofalowka
    {
        IDisplay display;
        public Mikrofalowka(IDisplay pokaz)
        {
            this.display = pokaz;
        }
        void IMikrofalowka.WlaczWylacz()
        {
            Console.WriteLine("Zmiana Stanu");
        }

        string IMikrofalowka.PowerOnOff(string power)
        {
            return "Włączona/Wyłączona";
        }
    }
}
